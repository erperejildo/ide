module.exports = {
  clearMocks: true,
  coverageDirectory: 'coverage',
  moduleNameMapper: {
    '^monaco-editor$': '<rootDir>/src/__mocks__/monaco-editor.tsx',
    '\\.(css|scss)$': 'identity-obj-proxy'
  },
  testMatch: ['**/?(*.)+(spec|test).[tj]s?(x)'],
  coveragePathIgnorePatterns: ['/node_modules/'],
  testPathIgnorePatterns: ['/node_modules/'],
  verbose: true,
  setupFilesAfterEnv: ['./src/setupTests.js'],
  testEnvironment: 'jsdom'
};
