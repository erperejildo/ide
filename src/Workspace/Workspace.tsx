import React from 'react';
import CodeEditor from '../components/CodeEditor/CodeEditor';
import FileExplorer from '../components/FileExplorer/FileExplorer';
import './Workspace.scss';
import ErrorBoundary from '../components/ErrorBoundary/ErrorBoundary';
import { Provider } from 'react-redux';
import store from '../store/store';

export const Workspace = () => {
  return (
    <ErrorBoundary>
      {/* <DndProvider backend={HTML5Backend}> */}
      <Provider store={store}>
        <div className="app-container">
          <FileExplorer />
          <CodeEditor />
        </div>
      </Provider>
      {/* </DndProvider> */}
    </ErrorBoundary>
  );
};
