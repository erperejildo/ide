import React from 'react';
import { render, waitFor, cleanup, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import CodeEditor from './CodeEditor';

const mockStore = configureStore([]);
const initialState = {
  files: [],
  selectedFile: null,
  theme: {
    darkMode: false
  }
};
const store = mockStore(initialState);
jest.mock('monaco-editor', () => ({
  editor: {
    create: jest.fn(),
    onDidChangeModelContent: jest.fn(),
    setValue: jest.fn(),
    dispose: jest.fn(),
    setTheme: jest.fn(),
    setModelLanguage: jest.fn()
  },
  editorWillUnmount: jest.fn(),
  setTheme: jest.fn()
}));
jest.mock('monaco-editor/esm/vs/editor/editor.api', () => ({
  languages: {
    getLanguages: jest.fn(() => [
      {
        extensions: ['.js'],
        id: 'javascript'
      }
    ])
  }
}));

describe('CodeEditor component', () => {
  afterEach(() => {
    cleanup();
    jest.clearAllMocks();
  });

  test('renders CodeEditor component', () => {
    render(
      <Provider store={store}>
        <CodeEditor />
      </Provider>
    );

    expect(screen.getByText('IDE')).toBeInTheDocument();
  });
});
