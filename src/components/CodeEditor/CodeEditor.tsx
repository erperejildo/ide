import React, { useEffect, useRef } from 'react';
import * as monaco from 'monaco-editor';
import { languages } from 'monaco-editor/esm/vs/editor/editor.api';
import { useDispatch, useSelector } from 'react-redux';
import './CodeEditor.scss';
import { setFiles, updateFiles } from '../../store/actions';
import DarkModeButton from '../DarkModeButton/DarkModeButton';
import { DefaultFile } from '../../types/defaultFile';
import { StateData } from '../../types/stateData';

const CodeEditor: React.FC = () => {
  const dispatch = useDispatch();
  const editorRef = useRef<HTMLDivElement | null>(null);
  const files = useSelector((state: StateData) => state.files);
  const selectedFile = useSelector((state: StateData) => state.selectedFile);
  const darkMode = useSelector((state: StateData) => state.theme.darkMode);

  useEffect(() => {
    if (!selectedFile) return;

    let editor: monaco.editor.IStandaloneCodeEditor | null = null;

    if (editorRef.current) {
      editor = monaco.editor.create(editorRef.current, {
        language: 'plaintext',
        theme: darkMode ? 'vs-dark' : 'vs-light',
        automaticLayout: true
      });

      if (selectedFile.filePath) {
        supportExtension(editor);
        editor.setValue(selectedFile.contents);
      }

      editor.onDidChangeModelContent(() => {
        const content = editor?.getValue() || '';
        const updatedState = files.map((file: DefaultFile) =>
          file.path === selectedFile.filePath + selectedFile.name
            ? {
                ...file,
                name: selectedFile.name,
                contents: content
              }
            : file
        );
        // this is simple enough to have both dispatch together,
        // but just in case we need extra logic...
        dispatch(updateFiles(updatedState));
        dispatch(setFiles(updatedState));
      });

      return () => {
        if (editor) {
          editor.dispose();
        }
      };
    }
  }, [selectedFile, dispatch]);

  useEffect(() => {
    monaco.editor.setTheme(darkMode ? 'vs-dark' : 'vs-light');
  }, [darkMode]);

  function supportExtension(editor: any) {
    const fileExtension = selectedFile.name.split('.').pop();
    if (fileExtension) {
      const languageId: any = languages
        .getLanguages()
        .find(lang => lang.extensions?.includes(`.${fileExtension}`))?.id;
      monaco.editor.setModelLanguage(editor?.getModel()!, languageId);
    }
  }

  return (
    <div className="code-editor-container">
      <div className={`code-editor-header ${darkMode ? 'dark' : 'light'}`}>
        <h1>IDE</h1>
        <DarkModeButton />
      </div>
      <div className="code-editor" ref={editorRef} />
    </div>
  );
};

export default CodeEditor;
