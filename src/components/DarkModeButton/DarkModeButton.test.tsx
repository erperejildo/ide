import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import DarkModeButton from './DarkModeButton';

const mockStore = configureStore([]);
const initialState = {
  theme: {
    darkMode: false
  }
};
const store = mockStore(initialState);

describe('DarkModeButton component', () => {
  it('renders DarkModeButton component and toggles dark mode', () => {
    render(
      <Provider store={store}>
        <DarkModeButton />
      </Provider>
    );

    const themeIcon = screen.getByAltText('Theme Icon');

    expect(themeIcon).toBeInTheDocument();

    fireEvent.click(themeIcon);

    const actions = store.getActions();
    expect(actions).toHaveLength(1);
    expect(actions[0].type).toEqual('TOGGLE_DARK_MODE');
  });
});
