import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toggleDarkMode } from '../../store/actions';
import './DarkModeButton.scss';
import { StateData } from '../../types/stateData';

const DarkModeButton: React.FC = () => {
  const dispatch = useDispatch();
  const darkMode = useSelector((state: StateData) => state.theme.darkMode);

  const handleToggle = () => {
    dispatch(toggleDarkMode(!darkMode));
  };

  return (
    <img
      className="theme-icon"
      src={`src/images/${darkMode ? 'light' : 'dark'}.svg`}
      alt="Theme Icon"
      onClick={handleToggle}
    />
  );
};

export default DarkModeButton;
