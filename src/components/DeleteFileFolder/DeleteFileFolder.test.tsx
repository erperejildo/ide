import React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import DeleteFileFolder from './DeleteFileFolder';
import { NodeTree } from '../../types/nodeTree';

const mockStore = configureStore([]);
const initialState = {
  files: [
    { path: 'app/file1.txt', contents: 'File 1 content' },
    { path: 'app/folder1/', contents: null, type: 'folder', children: [] }
  ]
};
const store = mockStore(initialState);

describe('DeleteFileFolder component', () => {
  it('renders DeleteFileFolder component and triggers deletion', async () => {
    const nodeTree: NodeTree = {
      name: 'file1.txt',
      type: 'file',
      contents: 'File 1 content',
      children: []
    };

    const basePath = 'app/';

    render(
      <Provider store={store}>
        <DeleteFileFolder nodeTree={nodeTree} basePath={basePath} />
      </Provider>
    );

    fireEvent.click(screen.getByAltText(`Delete ${nodeTree.type}`));

    expect(screen.getByText(`Delete ${nodeTree.type}`)).toBeInTheDocument();

    fireEvent.click(screen.getByText('Delete'));

    await waitFor(() => {
      expect(
        // change ${nodeTree.type} to folder to force an error
        screen.getByText(`${nodeTree.name} ${nodeTree.type} deleted.`)
      ).toBeInTheDocument();
    });

    const actions = store.getActions();
    expect(actions).toHaveLength(1);
    expect(actions[0].type).toEqual('UPDATE_FILES');
  });
});
