import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './DeleteFileFolder.scss';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import DialogContentText from '@mui/material/DialogContentText';
import { NodeTree } from '../../types/nodeTree';
import { updateFiles } from '../../store/actions';
import Snackbar from '@mui/material/Snackbar';
import { DefaultFile } from '../../types/defaultFile';
import { StateData } from '../../types/stateData';

export interface DeleteFileFolderProps {
  nodeTree: NodeTree;
  basePath: string;
}

const DeleteFileFolder: React.FC<DeleteFileFolderProps> = ({
  nodeTree,
  basePath
}) => {
  const dispatch = useDispatch();
  const [openDialog, setOpenDialog] = React.useState<boolean>(false);
  const [openSnackbar, setOpenSnackbar] = React.useState<boolean>(false);
  const [deleteMsg, setDeleteMsg] = React.useState<string>('');
  const files = useSelector((state: StateData) => state.files);

  const handleClick = () => {
    setOpenDialog(true);
  };

  const handleDialogOption = (val: boolean) => {
    if (!val) {
      return setOpenDialog(false);
    }

    setDeleteMsg(`${nodeTree.name} ${nodeTree.type} deleted.`);
    const updatedFiles = files.filter((file: DefaultFile) => {
      if (!file.path.includes(basePath + nodeTree.name)) {
        return file;
      }
    });
    setOpenDialog(false);
    setOpenSnackbar(true);
    dispatch(updateFiles(updatedFiles));
  };

  return (
    <div className="delete-container">
      <img
        className="delete-icon "
        src="src/images/delete.svg"
        alt={`Delete ${nodeTree.type}`}
        onClick={handleClick}
      />

      <Dialog
        open={openDialog}
        onClose={handleDialogOption}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <DialogTitle id="alert-dialog-title">
          Delete {nodeTree.type}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {nodeTree.type === 'folder'
              ? `This will delete ${nodeTree.name} folder and all its content, including other folders and files inside.`
              : `This will delete ${nodeTree.name} file.`}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleDialogOption(false)}>Cancel</Button>
          <Button
            onClick={() => handleDialogOption(true)}
            color="error"
            autoFocus>
            Delete
          </Button>
        </DialogActions>
      </Dialog>

      <Snackbar
        open={openSnackbar}
        autoHideDuration={3000}
        message={deleteMsg}
      />
    </div>
  );
};

export default DeleteFileFolder;
