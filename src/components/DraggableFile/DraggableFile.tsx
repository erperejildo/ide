import React from 'react';
import { useDrag } from 'react-dnd';
import './DraggableFile.scss';

const DraggableFile: React.FC<any> = ({ file }) => {
  const [, drag] = useDrag({
    type: 'FILE',
    item: { type: 'FILE', file }
  });

  return (
    <div ref={drag} style={{ cursor: 'move' }}>
      {file.name}
    </div>
  );
};

export default DraggableFile;
