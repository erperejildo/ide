import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import FileExplorer from './FileExplorer';

const mockStore = configureStore([]);
const store = mockStore({
  files: [
    { path: 'app/file1.txt', contents: 'File 1 contents' },
    { path: 'app/file2.txt', contents: 'File 2 contents' },
    { path: 'app/folder/file3.txt', contents: 'File 3 contents' }
  ],
  theme: { darkMode: false },
  searchFile: ''
});

describe('FileExplorer', () => {
  it('renders FileExplorer component', () => {
    render(
      <Provider store={store}>
        <FileExplorer />
      </Provider>
    );

    expect(screen.getByText('File Explorer')).toBeInTheDocument();
    expect(screen.getByText('file2.txt')).toBeInTheDocument();
  });
});
