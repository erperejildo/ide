import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './FileExplorer.scss';
import { selectFile, setFiles } from '../../store/actions';
import defaultFiles from '../../defaultFiles';
import SearchBar from '../SearchBar/SearchBar';
import NewFileFolder from '../NewFileFolder/NewFileFolder';
import DeleteFileFolder from '../DeleteFileFolder/DeleteFileFolder';
import { NodeTree } from '../../types/nodeTree';
import { DefaultFile } from '../../types/defaultFile';
import { StateData } from '../../types/stateData';

const FileExplorer: React.FC = () => {
  const dispatch = useDispatch();
  const files = useSelector((state: StateData) => state.files);
  const [folderState, setFolderState] = useState<{ [folder: string]: boolean }>(
    {}
  );
  const [showMenu, setShowMenu] = useState<boolean>(true);
  const [nodeTree, setNodeTree] = useState<any>(null);
  const darkMode = useSelector((state: StateData) => state.theme.darkMode);
  const searchFile = useSelector((state: StateData) => state.searchFile);
  // const [dropTarget, setDropTarget] = useState<string>('');

  useEffect(() => {
    dispatch(setFiles(defaultFiles));
  }, [dispatch]);

  useEffect(() => {
    if (!files) return;

    setNodeTree(buildnodeTree(files, searchFile));
  }, [files, searchFile]);

  const toggleFolder = (folder: string) => {
    setFolderState(prevState => ({
      ...prevState,
      [folder]: !prevState[folder]
    }));
  };

  const getImageForFileType = (path: string): string | null => {
    const fileExtension = path.split('.').pop();
    if (fileExtension) {
      switch (fileExtension.toLowerCase()) {
        case 'tsx' || 'ts':
          return 'tsx';
        case 'js' || 'json':
          return 'js';
        case 'css':
          return 'css';
        case 'html':
          return 'html';
        default:
          return 'question-mark';
      }
    }
    return null;
  };

  const buildnodeTree = (fileList: any, searchFile: string) => {
    const nodeTree: NodeTree = {
      name: 'Root',
      contents: '',
      type: 'folder',
      children: []
    };

    fileList.forEach((file: DefaultFile) => {
      const pathSegments = file.path.split('/');
      let currentFolder = nodeTree;

      pathSegments.forEach((segment: string, index: number) => {
        const isLastSegment = index === pathSegments.length - 1;
        const existingFolder = currentFolder.children.find(
          (child: NodeTree) => child.name === segment
        );

        if (!segment) return;

        if (existingFolder) {
          currentFolder = existingFolder;
        } else {
          const nodeTree: NodeTree = {
            name: segment,
            contents: file.contents,
            type: isLastSegment ? 'file' : 'folder',
            children: []
          };

          if (
            nodeTree.type === 'file' &&
            !nodeTree.name.toLowerCase().includes(searchFile.toLowerCase())
          )
            return;

          currentFolder.children.push(nodeTree);
          currentFolder = nodeTree;
        }
      });
    });

    return nodeTree;
  };

  // couldn't make the drag&drop work :_(
  // const handleDrop = useCallback(
  //   (file: any) => {
  //     if (dropTarget) {
  //       const newPath = dropTarget + file.name;
  //     }
  //   },
  //   [dropTarget]
  // );
  // const [, drop] = useDrop({
  //   accept: 'FILE',
  //   drop: (item: any) => handleDrop(item.file)
  // });

  if (!nodeTree) return null;

  const renderFilesAndFolders = (node: any, basePath = 'app/') => {
    const sortedChildren = node.children.slice().sort((a: any, b: any) => {
      if (a.type === b.type) {
        return a.name.localeCompare(b.name);
      }
      return a.type === 'folder' ? -1 : 1;
    });

    return (
      <div
        className="tree-node"
        // ref={drop}
      >
        {!node.children.length && (
          <div className="file-or-folder empty">empty</div>
        )}
        {sortedChildren.map((child: any, index: number) => (
          <div key={index} className="tree-element">
            <div
              className={`file-or-folder ${child.type}`}
              onClick={() => {
                if (child.type === 'folder') {
                  toggleFolder(`${basePath}${child.name}/`);
                } else {
                  dispatch(selectFile(basePath, child.name, child.contents));
                }
              }}>
              {getImageForFileType(child.type) && (
                <img
                  className="file-icon"
                  src={`src/images/${
                    child.type === 'file'
                      ? getImageForFileType(child.name)
                      : folderState[`${basePath}${child.name}/`]
                      ? 'folder-down'
                      : 'folder-right'
                  }.svg`}
                  alt={`${child.type} Icon`}
                />
              )}
              {child.name}
              <DeleteFileFolder nodeTree={child} basePath={basePath} />
            </div>
            {child.type === 'folder' &&
              folderState[`${basePath}${child.name}/`] &&
              renderFilesAndFolders(child, `${basePath}${child.name}/`)}
          </div>
        ))}
        <div className="file-or-folder empty">
          <NewFileFolder path={basePath} />
        </div>
      </div>
    );
  };

  const handleMenuToggle = () => {
    setShowMenu(!showMenu);
  };

  return (
    <div className={`file-explorer-container ${darkMode ? 'dark' : 'light'}`}>
      <img
        className="menu-icon"
        src="src/images/menu.svg"
        alt="Toggle Menu"
        onClick={handleMenuToggle}></img>
      {showMenu && (
        <div className="file-explorer-hidden">
          <h2>File Explorer</h2>
          <SearchBar />
          <div className="file-explorer-tree">
            {renderFilesAndFolders(nodeTree.children[0])}
          </div>
          {/* d&d */}
          {/* {child.type === 'file' && (
              <DraggableFile
                file={child}
                onDrop={() => handleDrop(child)}
                setDropTarget={setDropTarget}
              />
            )} */}
        </div>
      )}
    </div>
  );
};

export default FileExplorer;
