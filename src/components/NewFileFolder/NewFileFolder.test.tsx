import React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import NewFileFolder from './NewFileFolder';

const mockStore = configureStore();

describe('NewFileFolder component', () => {
  it('renders NewFileFolder component', () => {
    const store = mockStore({ files: [] });

    render(
      <Provider store={store}>
        <NewFileFolder path="test/" />
      </Provider>
    );

    expect(screen.getByTestId('create-button')).toBeInTheDocument();
  });

  it('opens the menu and selects "Folder"', async () => {
    const store = mockStore({ files: [] });

    render(
      <Provider store={store}>
        <NewFileFolder path="test/" />
      </Provider>
    );

    fireEvent.click(screen.getByTestId('create-button'));
    await waitFor(() => screen.getByTestId('folder-option'));

    fireEvent.click(screen.getByTestId('folder-option'));
    expect(screen.getByText('Create new folder')).toBeInTheDocument();
  });

  it('opens the dialog and creates a new folder', async () => {
    const store = mockStore({ files: [] });

    render(
      <Provider store={store}>
        <NewFileFolder path="test/" />
      </Provider>
    );

    fireEvent.click(screen.getByTestId('create-button'));
    await waitFor(() => screen.getByTestId('folder-option'));

    fireEvent.click(screen.getByTestId('folder-option'));
    expect(screen.getByLabelText('Create new folder')).toBeInTheDocument();

    fireEvent.change(screen.getByLabelText('Name'), {
      target: { value: 'NewFolder' }
    });
    fireEvent.click(screen.getByTestId('create-button-confirm'));
  });
});
