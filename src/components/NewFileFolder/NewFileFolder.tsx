import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './NewFileFolder.scss';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import DialogContentText from '@mui/material/DialogContentText';
import TextField from '@mui/material/TextField';
import { updateFiles } from '../../store/actions';
import { StateData } from '../../types/stateData';

interface NewFileFolderProps {
  path: string;
}

const NewFileFolder: React.FC<NewFileFolderProps> = ({ path }) => {
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const openOptions = Boolean(anchorEl);
  const [openDialog, setOpenDialog] = React.useState<boolean>(false);
  const [option, setOption] = React.useState<string>('');
  const [inputVal, setInputVal] = React.useState<string>('');
  const [error, setError] = React.useState<boolean>(false);
  const files = useSelector((state: StateData) => state.files);

  const handleMenuClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleOption = (type: string) => {
    setOption(type);
    setAnchorEl(null);
    setOpenDialog(true);
  };

  const handleDialogOption = (val: boolean) => {
    if (!val) {
      setOpenDialog(false);
      return setError(false);
    }

    if (!inputVal.length) {
      return setError(true);
    }

    let name = path + inputVal;
    if (option === 'folder') name = name + '/';
    const updatedFiles = [...files, { path: name, contents: '' }];
    dispatch(updateFiles(updatedFiles));

    setInputVal('');
    handleDialogOption(false);
  };

  const handleInputChange = (e: any) => {
    setInputVal(e.target.value);
  };

  return (
    <div>
      <Button
        id="basic-button"
        aria-controls={openOptions ? 'basic-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={openOptions ? 'true' : undefined}
        onClick={handleMenuClick}>
        <img
          data-testid="create-button"
          className="file-icon"
          src="src/images/new.svg"
          alt="New File/Folder menu"
        />{' '}
        Create
      </Button>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={openOptions}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button'
        }}>
        <MenuItem
          data-testid="folder-option"
          onClick={() => handleOption('folder')}>
          Folder
        </MenuItem>
        <MenuItem
          data-testid="file-button"
          onClick={() => handleOption('file')}>
          File
        </MenuItem>
      </Menu>

      <Dialog
        open={openDialog}
        onClose={handleDialogOption}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <DialogTitle id="alert-dialog-title">Create new {option}</DialogTitle>
        <DialogContent>
          <TextField
            id="filled-basic"
            label="Name"
            variant="filled"
            size="small"
            onChange={handleInputChange}
            error={error}
            helperText={error ? 'Add a name' : ''}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleDialogOption(false)}>Cancel</Button>
          <Button
            data-testid="create-button-confirm"
            onClick={() => handleDialogOption(true)}
            autoFocus>
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default NewFileFolder;
