import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import SearchBar from './SearchBar';
import { Provider } from 'react-redux';

const mockStore = configureMockStore();
const store = mockStore();

describe('SearchBar component', () => {
  it('dispatches searchFiles action on input change', () => {
    render(
      <Provider store={store}>
        <SearchBar />
      </Provider>
    );

    const searchInput = screen.getByLabelText('Search file...');

    fireEvent.change(searchInput, { target: { value: 'test' } });

    expect(store.getActions()).toEqual([
      { type: 'SEARCH_FILES', payload: 'test' }
    ]);
  });
});
