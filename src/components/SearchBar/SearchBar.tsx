import React from 'react';
import { useDispatch } from 'react-redux';
import './SearchBar.scss';
import TextField from '@mui/material/TextField';
import { searchFiles } from '../../store/actions';

const SearchBar: React.FC = () => {
  const dispatch = useDispatch();

  const handleChange = (e: any) => {
    const value = e.target.value;
    dispatch(searchFiles(value));
  };

  return (
    <div className="search-bar">
      <TextField
        label="Search file..."
        variant="outlined"
        size="small"
        onChange={handleChange}
      />
    </div>
  );
};

export default SearchBar;
