import { DefaultFile } from '../types/defaultFile';

export const setFiles = (files: DefaultFile[]) => {
  try {
    const storedFiles = sessionStorage.getItem('ide-files');
    return {
      type: 'LOAD_FILES',
      payload: storedFiles ? JSON.parse(storedFiles) : files
    };
  } catch (error) {
    console.error('Error loading files from sessionStorage:', error);
    // I just loaded the default ones again, but we could show a proper error page
    return {
      type: 'LOAD_FILES',
      payload: files
    };
  }
};

export const updateFiles = (files: DefaultFile[]) => {
  try {
    sessionStorage.setItem('ide-files', JSON.stringify(files));
    return {
      type: 'UPDATE_FILES',
      payload: files
    };
  } catch (error) {
    console.error('Error loading files from sessionStorage:', error);
    // Again, we could should an error but here I'm just saving the default ones
    return {
      type: 'UPDATE_FILES',
      payload: files
    };
  }
};

export const selectFile = (
  filePath: string,
  name: string,
  contents: string
) => ({
  type: 'SELECT_FILE',
  payload: { filePath, name, contents }
});

export const searchFiles = (searchFile: string) => {
  return {
    type: 'SEARCH_FILES',
    payload: searchFile
  };
};

export const toggleDarkMode = (darkMode: boolean) => {
  return {
    type: 'TOGGLE_DARK_MODE',
    payload: { darkMode: darkMode }
  };
};
