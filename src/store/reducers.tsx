import { combineReducers } from 'redux';
import { StateData } from '../types/stateData';

const initialState: StateData = {
  files: [],
  selectedFile: { filePath: null, name: '', contents: '' },
  searchFile: '',
  theme: { darkMode: false }
};

const fileReducer = (state = initialState.files, action: any) => {
  switch (action.type) {
    case 'LOAD_FILES':
      return action.payload;
    case 'UPDATE_FILES':
      return action.payload;
    default:
      return state;
  }
};

const selectedFileReducer = (
  state = initialState.selectedFile,
  action: any
) => {
  switch (action.type) {
    case 'SELECT_FILE':
      return {
        filePath: action.payload.filePath,
        name: action.payload.name,
        contents: action.payload.contents
      };
    default:
      return state;
  }
};

const searchFilesReducer = (state = initialState.searchFile, action: any) => {
  switch (action.type) {
    case 'SEARCH_FILES':
      return action.payload;
    default:
      return state;
  }
};

const themeReducer = (state = initialState.theme, action: any) => {
  switch (action.type) {
    case 'TOGGLE_DARK_MODE':
      return action.payload;
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  files: fileReducer,
  selectedFile: selectedFileReducer,
  searchFile: searchFilesReducer,
  theme: themeReducer
});

export default rootReducer;
