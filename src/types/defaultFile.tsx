export interface DefaultFile {
  path: string;
  contents: string;
}
