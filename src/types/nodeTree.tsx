export interface NodeTree {
  name: string;
  contents: string;
  type: string;
  children: NodeTree[];
}
