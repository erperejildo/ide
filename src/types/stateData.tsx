import { DefaultFile } from './defaultFile';

export interface StateData {
  files: DefaultFile[];
  selectedFile: SelectFile;
  searchFile: string;
  theme: Theme;
}

export interface SelectFile {
  filePath: string | null;
  name: string;
  contents: string;
}

export interface Theme {
  darkMode: boolean;
}
